﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using UnityEngine.Events;

public class ClickHandler : MonoBehaviour, IInputClickHandler {
    public UnityEvent unityEvent;

    public void OnInputClicked(InputClickedEventData eventData)
    {
        unityEvent.Invoke();
    }
}
