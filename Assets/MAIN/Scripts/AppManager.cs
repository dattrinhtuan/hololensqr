﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Examples.InteractiveElements;
using HoloToolkit.Unity;

public class AppManager : MonoBehaviour {
    public GameObject realModelAndUI;
    public GameObject realModel;
    public GameObject arModel;


    public SliderGestureControl resizeControl;
    public SliderGestureControl rotateControl;


    private readonly Vector3 STANDARD_SCALE = new Vector3(1f, 1f, 1f);


    private TextToSpeech tts;


    public static AppManager Instance
    {
        get;
        private set;
    }

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(this.gameObject);
    }

    private void Start()
    {
        tts = gameObject.GetComponent<TextToSpeech>();
        tts.StartSpeaking("Welcome to our application. It is a combination of augmented reality and mixed reality experience. " +
                          "Please take a look at one of our available images. " +
                          "When an augmented model pop up from the image, tap on that model."
                         );
    }

    public void ClickOnARModel()
    {
        // Save position of the AR model and Stop Vuforia
        var currentPos = arModel.transform.position;
        var currentScale = arModel.transform.lossyScale;
        if (TrackerManager.Instance.GetTracker<ObjectTracker>() != null)
            TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();

        // Show the real model and UI at the saved position and (re)scale it to the scale of AR model
        realModelAndUI.SetActive(true);
        realModelAndUI.transform.position = currentPos;
        realModelAndUI.transform.localScale = currentScale;

        // This new position is temporary because it will be changed later due to TapToPlace script
        Vector3 newTemporaryPosition = Camera.main.transform.localPosition
                                             + Camera.main.transform.forward * 5 // 5 meters forward
                                             + Camera.main.transform.up * 3; // 3 meters up
        StartCoroutine(MoveToPositionAndScale(realModelAndUI, newTemporaryPosition, STANDARD_SCALE, 2)); // animate in 2 seconds

        //Stop the bug that realModel continuosly run into us
        realModelAndUI.GetComponent<TapToPlaceOnSurface>().enabled = false;
    }
    private IEnumerator MoveToPositionAndScale(GameObject objectToMoveAndScale, Vector3 newPosition, Vector3 newLocalScale, float time)
    {
        var currentPos = objectToMoveAndScale.transform.position;
        var currentScale = objectToMoveAndScale.transform.localScale;
        var t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / time;
            objectToMoveAndScale.transform.position = Vector3.Lerp(currentPos, newPosition, t);
            objectToMoveAndScale.transform.localScale = Vector3.Lerp(currentScale, newLocalScale, t);
            yield return null;
        }

        // At the end of the animation, we enable TapToPlace script, and set isBeingPlaced as true, so that we dont have to click on the object any more
        TapToPlaceOnSurface tapToplace = realModelAndUI.GetComponent<TapToPlaceOnSurface>();
        tapToplace.enabled = true;
        tapToplace.IsBeingPlaced = true;

        // Activate Spatial Mapping
        tapToplace.AllowMeshVisualizationControl = true;
        HoloToolkit.Unity.SpatialMapping.SpatialMappingManager.Instance.DrawVisualMeshes = true;
        HoloToolkit.Unity.SpatialMapping.SpatialMappingManager.Instance.StartObserver();

        tts.StartSpeaking("Look around to scan the environment. When the scanning process finishes, try to place the model onto a surface");
    }



    /// <summary>
    /// Put model at a surface and start to view it in details
    /// </summary>
    /// <param name="position">Position to put the real model.</param>
    public void EnterRealModelViewer(Vector3 position)
    {
        realModelAndUI.transform.position = position;

        // De-Activate Spatial Mapping
        realModelAndUI.GetComponent<TapToPlaceOnSurface>().AllowMeshVisualizationControl = false;
        HoloToolkit.Unity.SpatialMapping.SpatialMappingManager.Instance.DrawVisualMeshes = false;
        HoloToolkit.Unity.SpatialMapping.SpatialMappingManager.Instance.StopObserver();
    }
    public void ExitRealModelViewer() {
        // Reset the Real Model and sliders. The rotate slider is a centered slider
        // Centered Slider has bug now, and when we set its slider value, the affect is wrong. So we ignore and do not reset it.
        rotateControl.SetSliderValue(0);
        realModel.transform.rotation = new Quaternion();

        if (realModelAndUI != null)
            realModelAndUI.SetActive(false);

        if (TrackerManager.Instance.GetTracker<ObjectTracker>() != null)
            TrackerManager.Instance.GetTracker<ObjectTracker>().Start();

        tts.StartSpeaking("Now we are back to the augmented reality mode and you can try to play with other images!");
    }




    public void Resize()
    {
        float value = resizeControl.SliderValue;
        if (value > 0)
            value = value + 1;
        else
            value = 1 / (1 - value);
        realModel.transform.localScale = value * STANDARD_SCALE;
    }
    public void Rotate()
    {
        int value = (int)rotateControl.SliderValue;
        realModel.transform.rotation = Quaternion.AngleAxis(value, Vector3.up);
    }
}
