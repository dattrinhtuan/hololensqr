﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity;
using UnityEngine.UI;

public class ReadAloud : MonoBehaviour {
    public Text textContent;

    public void ReadAloudText(){
        Debug.Log("Speak now ...");
        TextToSpeech tts = AppManager.Instance.GetComponent<TextToSpeech>();
        tts.Voice = TextToSpeechVoice.Mark;
        tts.StartSpeaking(textContent.text);
    }
}
