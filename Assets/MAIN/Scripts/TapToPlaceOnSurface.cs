﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using HoloToolkit.Unity.SpatialMapping;

public class TapToPlaceOnSurface : TapToPlace
{
    public override void OnInputClicked(InputClickedEventData eventData)
    { 
        if (IsBeingPlaced && SpatialMappingManager.Instance != null)
        {
            RaycastHit hitInfo;

            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hitInfo, 30.0f, SpatialMappingManager.Instance.LayerMask))
            {
                base.OnInputClicked(eventData);
                AppManager.Instance.EnterRealModelViewer(hitInfo.point);
            }
        }
    }
}