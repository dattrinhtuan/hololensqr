﻿using HoloToolkit.Unity;
using HoloToolkit.Unity.InputModule;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SpeechHandler : MonoBehaviour, ISpeechHandler
{
    public void OnSpeechKeywordRecognized(SpeechEventData eventData)
    {
        SelectARModel(eventData.RecognizedText);
    }

    public void SelectARModel(string command)
    {
        if (command.ToLower().Equals("select"))
        {
            StateManager sm = TrackerManager.Instance.GetStateManager();

            IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours();
            bool flag = false;
            foreach (TrackableBehaviour tb in activeTrackables)
            {
               if (tb.TrackableName.Equals("QR1"))
                {
                    flag = true;
                    break;
                }
                break;
            }

            if (flag)
            {
                AppManager.Instance.ClickOnARModel();
            }
        }
    }
}
